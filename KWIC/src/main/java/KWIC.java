import java.util.*;
public class KWIC {
    public ArrayList<String> get_title(ArrayList<String> all_text){
        ArrayList<String> title = new ArrayList<String>();
        int mark = 0;
        while(all_text.get(mark)!="::"){
            mark++;
        }
        mark++;
        for(int i = mark; i<all_text.size(); i++){
            title.add(all_text.get(i));
        }
        return title;
    }
    public ArrayList<String> get_ignore(ArrayList<String> all_text){
        ArrayList<String> ignore = new ArrayList<String>();
        int mark = 0;
        while(all_text.get(mark)!="::"){
            ignore.add(all_text.get(mark));
            mark++;
        }
        return ignore;
    }
    public ArrayList<String> get_key(ArrayList<String> all_text){
        ArrayList<String> ignore = get_ignore(all_text);
        ArrayList<String> title = get_title(all_text);
        ArrayList<String> key = new ArrayList<String>();
        int mark = 0;
        for(int i=0;i<title.size();i++){
            String single_title = title.get(i);
            for(String j : single_title.split(" ")){
                key.add(j);
            }
        }
        for(int i=0;i<ignore.size();i++){
            for(int j=0;j< key.size();j++){
                if(ignore.get(i).compareToIgnoreCase(key.get(j))==0){
                    key.remove(j);
                    j--;
                }
            }
        }

        HashSet<String> hs=new HashSet<>();
        hs.addAll(key);
        key.clear();
        key.addAll(hs);
        key.sort(String::compareToIgnoreCase);

        return key;
    }
    public ArrayList<String> get_sample_out(ArrayList<String> all_text){
        ArrayList<String> title = get_title(all_text);
        ArrayList<String> key = get_key(all_text);
        ArrayList<String> output = new ArrayList<String>();
        for(int i=0;i<key.size();i++){
            for(int j=0;j< title.size();j++){
                if(title.get(j).contains(key.get(i))){
                    String []str_temp = title.get(j).split(" ");
                    String str_output = "";
                    int count_key=0;
                    for(int k=0; k<str_temp.length;k++){
                        if(str_temp[k].compareToIgnoreCase(key.get(i))==0){
                            count_key++;
                        }
                    }
                    for(int k=0; k<str_temp.length;k++){
                        if(str_temp[k].compareToIgnoreCase(key.get(i))!=0){
                            str_output += str_temp[k].toLowerCase() + " ";
                        }else{
                            str_output += str_temp[k].toUpperCase() + " ";
                        }
                    }
                    str_output=str_output.substring(0, str_output.length() - 1);
                    if(count_key==1){
                        output.add(str_output);
                    }else{
                        int count_out = 0;
                        while(count_out!=count_key){
                            String [] n_out =  str_output.split(" ");
                            str_output = "";
                            for(int m =0;m< n_out.length;m++){
                                n_out[m] = n_out[m].toLowerCase();
                            }
                            int count_pr=0;
                            for(int m =0;m< n_out.length;m++){
                                if(n_out[m].compareToIgnoreCase(key.get(i))==0){
                                    count_pr++;
                                    if(count_pr-1 == count_out){
                                        n_out[m] = n_out[m].toUpperCase();
                                        break;
                                    }
                                }
                            }
                            for(int m =0;m< n_out.length;m++){
                                str_output += n_out[m] + " ";
                            }
                            str_output=str_output.substring(0, str_output.length() - 1);
                            output.add(str_output);
                            count_out++;
                        }
                    }

                }
            }
        }
        return output;
    }

    public static ArrayList<String> get_text() {
        ArrayList<String> all_text = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        while(input.hasNextLine()){
            String str = input.nextLine();
            if(str.equals("")){
                break;}
            all_text.add(str);
        }

        input.close();
        return all_text;
    }

    public static void main(String []args) {
        ArrayList<String> all_text = new ArrayList<>();
        all_text = get_text();
        System.out.println(all_text);
    }


}


